
echo "SAVING USER 1"
curl -v -X POST localhost:7000/user --header "Content-Type: application/json" --data @User1.json
echo ""
echo "----------------------------------------------------------"
echo "SAVING USER 2"
curl -v -X POST localhost:7000/user --header "Content-Type: application/json" --data @User2.json
echo ""
echo "----------------------------------------------------------"
echo "LISTING USERS"
curl -v -X GET localhost:7000/user
echo ""
echo "----------------------------------------------------------"
echo "DELETING USER 1"
curl -v -X DELETE localhost:7000/user/1
echo ""
echo "----------------------------------------------------------"
echo "LISTING USERS"
curl -v -X GET localhost:7000/user
echo ""
echo "----------------------------------------------------------"
echo "UPDATING USER 2"
curl -v -X POST localhost:7000/user/2 --header "Content-Type: application/json" --data @Update.json
echo ""
echo "----------------------------------------------------------"
echo "LISTING USERS"
curl -v -X GET localhost:7000/user
echo ""
echo "----------------------------------------------------------"
