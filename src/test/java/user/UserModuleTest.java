package user;

import com.google.inject.*;
import common.util.json.JsonMapper;
import io.javalin.http.Context;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import user.dao.UserDao;
import user.model.User;
import util.ResourceUtil;

import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserModuleTest extends TestCase {

  @InjectMocks
  private UserController controller;
  @Mock
  private UserDao userDao;
  @Inject
  JsonMapper mapper;
  @Mock
  ResourceUtil res;
  @Mock
  Context ctx;

  @Before
  public void setup() {
    Injector injector = Guice.createInjector(new AbstractModule() {
      @Override
      protected void configure() {

      }
    });
  }

  @Test
  public void testCreate() {
    //System.out.println(res);
    List<User> dummyUsers = res.resourceToList("users.json", User.class);

    when(ctx.body()).thenReturn("{}");
    controller.create(ctx);
    verify(ctx).status(404);
    assertEquals(0, userDao.all().size());

    when(ctx.body()).thenReturn(mapper.toJson(dummyUsers.get(0)));
    controller.create(ctx);
    verify(ctx).status(200);
    assertEquals(1, userDao.all().size());
  }

  @Test
  public void testUpdate() {

  }

  @Test
  public void testDelete() {

  }

}