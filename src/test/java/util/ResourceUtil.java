package util;

import com.fasterxml.jackson.core.JsonProcessingException;
import common.util.json.DefaultJsonMapper;

import javax.inject.Inject;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Collectors;

public class ResourceUtil {

  @Inject
  DefaultJsonMapper mapper;

  public String resourceToString(String path) {
    Scanner s = new Scanner(getClass().getResourceAsStream(path)).useDelimiter("\\A");
    return s.hasNext() ? s.next() : "";
  }

  public Object resourceToObject(String path) {
    Scanner s = new Scanner(getClass().getResourceAsStream(path)).useDelimiter("\\A");
    return mapper.fromJson(s.hasNext() ? s.next() : "{}", Object.class);
  }

  public <T> T resourceToInstance(String path, Class<T> type) {
    return mapper.fromJson(resourceToString(path), type);
  }

  public <T> List<T> resourceToList(String path, Class<T> type) {
    System.out.println("WHAT " + resourceToString(path));
    return (List<T>) mapper.fromJson(resourceToString(path), List.class).stream()
      .map(obj -> {
        System.out.println(obj);
        try {
          return mapper.treeToValue(mapper.valueToTree(obj), type);
        } catch (JsonProcessingException e) {
          e.printStackTrace();
        }
        return null;
      }).filter(Objects::nonNull).collect(Collectors.toList());
  }
}
