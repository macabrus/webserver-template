package auth;

import com.google.inject.Inject;
import common.routing.Router;
import io.javalin.Javalin;

import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.post;

public class AuthRouter extends Router<AuthController> {

  private Javalin app;

  @Inject
  public AuthRouter(Javalin app) {

    this.app = app;
  }

  @Override
  public void bindRoutes() {
    // if this module is installed, permissions are checked
    // before providing access to routes
    app.before(ctx -> {
      // TODO: check permissions
    });

    app.routes(()-> {
      //get("signin", new VueComponent("<signin-form></signin-form>"));
      get("signin", ctx -> ctx.result("TODO"));
      post("signin", ctx -> ctx.result("TODO"));
      //get("register", new VueComponent("<registration-form></registration-form>"));
      get("register", ctx -> ctx.result("TODO"));
      post("register", ctx -> ctx.result("TODO"));
    });
  }
}
