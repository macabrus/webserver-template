package auth;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;
import common.routing.Router;

public class AuthModule extends AbstractModule {

  @Override
  protected void configure() {
    //bind(AuthController.class);
    // Single instance for user management module for now, could use provider later
    //bind(JsonMapper.class).toInstance(new DefaultJsonMapper());
    //install(new UserServiceModule());
    Multibinder.newSetBinder(binder(), Router.class).addBinding().to(AuthRouter.class);
  }
}
