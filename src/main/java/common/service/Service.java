package common.service;

import java.util.concurrent.CompletableFuture;

public interface Service<I,O> {
  O compute(I input);
}
