package common.dao;

import java.util.List;
import java.util.Map;

public interface Dao<T> extends AutoCloseable {
  List<T> all();
  void save(T entity);
  T findById(long id);
  void delete(T entity);
  void deleteById(long id);
  void update(T entity);
  void update(Map<String, Object> entitiesMatching, Map<String, Object> setProperties);
}
