package common.dao;

import common.util.ListMap;
import common.util.json.DefaultJsonMapper;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.inject.Inject;
import javax.persistence.criteria.*;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;

public abstract class AbstractDao<T> implements Dao<T> {

  private Session session;
  private Transaction transaction;
  private Class<T> entityClass;
  @Inject
  DefaultJsonMapper mapper;

  public AbstractDao() {
    entityClass = (Class<T>)
      ((ParameterizedType)getClass().getGenericSuperclass())
        .getActualTypeArguments()[0];
    session = HibernateUtil.getSessionFactory().openSession();
  }

  @Override
  public void save(T entity) {
    begin();
    session.save(entity);
    commit();
  }

  @Override
  public void update(T entity) {
    session.update(entity);
  }

  @Override
  public List<T> all() {
    return findBy(null);
  }

  public List<T> findBy(Map<String, Object> properties) {
    if (properties == null)
      properties = new ListMap<>();
    CriteriaBuilder cb = session.getCriteriaBuilder();
    CriteriaQuery<T> cq = cb.createQuery(entityClass);
    Root<T> root = cq.from(entityClass);
    final Predicate[] where = {cb.and()};
    properties.forEach((k,v) -> {
      where[0] = cb.and(where[0],cb.equal(root.get(k), v));
    });
    Query<T> query = session.createQuery(cq.where(where[0]));
    return query.getResultList();
  }

  public int deleteBy(Map<String, Object> properties) {
    if (properties == null)
      properties = new ListMap<>();
    CriteriaBuilder cb = session.getCriteriaBuilder();
    CriteriaDelete<T> cq = cb.createCriteriaDelete(entityClass);
    Root<T> root = cq.from(entityClass);
    final Predicate[] where = {cb.and()};
    properties.forEach((k,v) -> {
      where[0] = cb.and(where[0],cb.equal(root.get(k), v));
    });
    begin();
    session.createQuery(cq.where(where)).executeUpdate();
    commit();
    return 1;
  }

  public void update(Map<String, Object> entitiesMatching, Map<String, Object> setProperties) {
    if (entitiesMatching == null)
      entitiesMatching = new ListMap<>();
    CriteriaBuilder cb = session.getCriteriaBuilder();
    final CriteriaUpdate<T>[] cq = new CriteriaUpdate[]{cb.createCriteriaUpdate(entityClass)};
    Root<T> root = cq[0].from(entityClass);
    final Predicate[] where = {cb.and()};
    entitiesMatching.forEach((k,v) -> {
      where[0] = cb.and(where[0],cb.equal(root.get(k), v));
    });
    begin();
    session.createQuery(cq[0].where(where)).executeUpdate();
    commit();
  }

  private void begin() {
    transaction = session.beginTransaction();
  }

  private void commit() {
    transaction.commit();
  }

  @Override
  public void close() throws Exception {
    if (transaction.isActive())
      transaction.rollback();
    session.close();
  }
}
