package common;

public interface AppEntryPoint {
  void boot(String[] args);
}