package common.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Memory efficient Map implementation suitable for very small collections (few pairs only)
 * CRUD operations are O(1) !!!
 * @param <K>
 * @param <V>
 */
public class ListMap<K, V> implements Map<K, V> {

  List<Entry<K,V>> pairList = new ArrayList<>();

  @Override
  public int size() {
    return pairList.size();
  }

  @Override
  public boolean isEmpty() {
    return pairList.isEmpty();
  }

  @Override
  public boolean containsKey(Object key) {
    return pairList.stream().anyMatch(e -> e.getKey().equals(key));
  }

  @Override
  public boolean containsValue(Object value) {
    return values().stream().anyMatch(val -> val.equals(value));
  }

  @Override
  public V get(Object key) {
    for (Entry<K,V> entry : pairList)
      if (entry.getKey().equals(key))
        return entry.getValue();
    return null;
  }

  @Nullable
  @Override
  public V put(K key, V value) {
    pairList.removeIf(e -> e.getKey().equals(key) && e.getValue().equals(value));
    pairList.add(new AbstractMap.SimpleEntry<>(key, value));
    return value;
  }

  @Override
  public V remove(Object key) {
    for (Entry<K,V> entry : pairList) {
      if (entry.equals(key)){
        pairList.remove(entry);
        return entry.getValue();
      }
    }
    return null;
  }

  @Override
  public void putAll(@NotNull Map<? extends K, ? extends V> m) {
    m.forEach(this::put);
  }

  @Override
  public void clear() {
    pairList.clear();
  }

  @NotNull
  @Override
  public Set<K> keySet() {
    return pairList.stream().map(Entry::getKey).collect(Collectors.toSet());
  }

  @NotNull
  @Override
  public Collection<V> values() {
    return pairList.stream().map(Entry::getValue).collect(Collectors.toList());
  }

  @NotNull
  @Override
  public Set<Entry<K, V>> entrySet() {
    return new HashSet<>(pairList);
  }
}
