package common.util;

import org.apache.commons.beanutils.BeanUtilsBean;

import java.lang.reflect.InvocationTargetException;

public class NullAwareBeanMapper extends BeanUtilsBean {

  @Override
  public void copyProperty(Object bean, String name, Object value)
    throws IllegalAccessException, InvocationTargetException {
    if(value==null){
      return; // Do nothing if null;
    }
    super.copyProperty(bean, name, value);
  }
}
