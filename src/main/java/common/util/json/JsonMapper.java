package common.util.json;

public interface JsonMapper {
  public String toJson(Object object);
  public <T> T fromJson(String json, Class<T> clazz);
}
