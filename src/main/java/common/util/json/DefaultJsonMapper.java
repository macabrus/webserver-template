package common.util.json;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class DefaultJsonMapper extends ObjectMapper implements JsonMapper {

  public DefaultJsonMapper() {
    super();
    setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
    registerModule(new JodaModule());
    //enable(SerializationFeature.INDENT_OUTPUT);
    //disable(com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    //setDefaultMergeable(true);
  }

  @Override
  public String toJson(Object object) {
    try {
      return writeValueAsString(object);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  public <T> T fromJson(String json, Class<T> clazz) {
    try {
      return readValue(json, clazz);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
    return null;
  }

  public <T> T fromJson(InputStream stream, Class<T> clazz) {
    try {
      return readValue(new InputStreamReader(stream), clazz);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }
}
