package common.util.json;

import com.google.inject.Provider;

public class JsonMapperProvider implements Provider<JsonMapper> {

  @Override
  public DefaultJsonMapper get() {
    return new DefaultJsonMapper();
  }
}
