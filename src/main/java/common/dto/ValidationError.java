package common.dto;

import lombok.Data;

public @Data
class ValidationError {
  public ValidationError(String property, String message) {
    this.property = property;
    this.message = message;
  }
  String message;
  String property;
}
