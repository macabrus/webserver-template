import com.google.inject.AbstractModule;
import web.WebModule;

public class AppModule extends AbstractModule {
  protected void configure() {
    bind(Startup.class);
    install(WebModule.create());
  }
}
