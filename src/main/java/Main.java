import com.google.inject.Inject;
import common.util.json.DefaultJsonMapper;
import io.javalin.Javalin;
import io.javalin.http.staticfiles.Location;
import io.javalin.plugin.rendering.vue.VueComponent;
import lombok.extern.java.Log;
import user.UserController;
import user.dao.DefaultUserDao;

import static io.javalin.apibuilder.ApiBuilder.crud;

@Log
public class Main {
	@Inject
	private static DefaultJsonMapper mapper;

	public static void main(String[] args) {
		// Persistence
		try(DefaultUserDao userDao = new DefaultUserDao()) {
			// Saving user
			// Fetching user back
			log.info("DB -> User: " + mapper.toJson(userDao.findByUsername("test")));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Servlet
		Javalin app = Javalin.create(config -> {
			config.enableWebjars();
			config.addStaticFiles("static", Location.CLASSPATH);
		}).start();
		// Clean stopping...
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			app.stop();
		}));
		// On stop events...
		app.events(event -> {
			event.serverStopping(() -> {
				log.info("Stopping server!");
			});
			event.serverStopped(() -> {
				log.info("Server stopped!");
			});
		});
		// Routes
		app.routes(()-> {
			crud("user/:user-id", new UserController());
		});
		app.get("/greet", new VueComponent("<greeting></greeting>"));
		app.get("/register", new VueComponent("<register></register>"));
		app.post("/register", ctx -> ctx.result(mapper.toJson(ctx.formParamMap())));
	}
}
