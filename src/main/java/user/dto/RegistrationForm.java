package user.dto;

import lombok.Data;

import javax.validation.constraints.*;

public @Data
class RegistrationForm {
  @NotNull(message = "constraint.username.null")
  @Size(min = 3, max = 30, message = "constraint.username.size")
  @Pattern(regexp="[A-Za-z0-9_]*", message = "constraint.username.pattern")
  private String username;
  @NotNull(message = "constraint.email.null")
  @Email(message = "constraint.email")
  private String email;
  @NotNull(message = "constraint.password.null")
  @Size(min = 8, message = "constraint.password.min")
  @Pattern(regexp = ".*[0-9]+.*", message = "constraint.password.pattern")
  private String password1;
  private String password2;
}
