package user.dto;

import lombok.Data;

import javax.validation.constraints.*;

public @Data
class LoginForm {
  @NotNull
  @Size(min = 3, max = 30, message = "constraint.username.size")
  @Pattern(regexp="[A-Za-z0-9_]*", message = "constraint.username.pattern")
  private String username;
  @NotNull
  @Size(min = 8, message = "constraint.password.min")
  @Pattern(regexp = ".*[0-9]+.*", message = "constraint.password.pattern")
  private String password;
}
