package user;

import common.dto.ValidationError;
import common.util.NullAwareBeanMapper;
import common.util.json.JsonMapper;
import io.javalin.apibuilder.CrudHandler;
import io.javalin.http.Context;
import lombok.extern.java.Log;
import org.jetbrains.annotations.NotNull;
import user.dao.UserDao;
import user.dto.LoginForm;
import user.dto.RegistrationForm;
import user.model.User;
import user.service.DefaultLoginValidatorService;
import user.service.DefaultRegistrationValidatorService;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Log
public class UserController implements CrudHandler {

  @Inject
  UserDao userDao;
  @Inject
  DefaultRegistrationValidatorService registrationService;
  @Inject
  DefaultLoginValidatorService loginService;
  @Inject
  JsonMapper mapper;
  @Inject
  NullAwareBeanMapper beanMapper;

  @Override
  public void create(@NotNull Context context) {
    loginService.compute(new LoginForm());
    RegistrationForm form = context.bodyAsClass(RegistrationForm.class);
    List<ValidationError> errors = registrationService.compute(form);
    if (errors.isEmpty()) {
      User u = new User(form);
      userDao.save(u);
      context.status(200).json(u);
    }
    else {
      context.status(405).json(errors);
    }
  }

  @Override
  public void getAll(@NotNull Context context) {
    context.json(userDao.all());
  }

  @Override
  public void getOne(@NotNull Context context, @NotNull String s) {
    context.json(userDao.findById(Long.parseLong(s)));
  }

  @Override
  public void update(@NotNull Context context, @NotNull String s) {
    try {
      User u = userDao.findById(Long.parseLong(s));
      beanMapper.copyProperties(u, context.bodyAsClass(User.class));
      userDao.save(u);
//      userDao.update(
//        new ListMap<String, Object>(){{put("id", Long.parseLong(s));}},
//        context.bodyAsClass(User.class)
//      );
      context.status(200).json(u);
    } catch (Exception e) {
      e.printStackTrace();
      List<ValidationError> errors = new ArrayList<>();
      errors.add(new ValidationError(null,"error.user.update"));
      context.status(405).json(errors);
    }
  }

  @Override
  public void delete(@NotNull Context context, @NotNull String s) {
    try {
      //User u = userDao.findById(Long.parseLong(s));
      //userDao.deleteById(u.getId());
      userDao.deleteById(Long.parseLong(s));
      context.status(200).result("");
    } catch (Exception e) {
      e.printStackTrace();
      List<ValidationError> errors = new ArrayList<>();
      errors.add(new ValidationError(null,"error.user.delete"));
      context.status(405).json(errors);
    }
  }
}
