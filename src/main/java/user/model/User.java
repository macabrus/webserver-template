package user.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sun.istack.NotNull;
import lombok.Data;
import user.dto.RegistrationForm;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "user")
@JsonIgnoreProperties(ignoreUnknown = true)
public @Data
class User {
  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private Long id;
  @NotNull
  private String username;
  @NotNull
  @Enumerated(EnumType.ORDINAL)
  private Role role;
  @Column(columnDefinition = "text")
  @Convert(converter = UserAttributesConverter.class)
  private UserAttributes attrs;
  @NotNull
  private String password;
  private Instant dateCreated;
  private boolean confirmed = false;
  private boolean active = false;

  public User(RegistrationForm form) {
    this.username = form.getUsername();
    this.role = Role.BASIC;
    this.password = form.getPassword1();
    this.dateCreated = Instant.now();
    this.attrs = new UserAttributes();
  }

  public User(){}
}
