package user.model;

import common.util.json.DefaultJsonMapper;
import lombok.SneakyThrows;

import javax.persistence.AttributeConverter;

public class UserAttributesConverter implements AttributeConverter<UserAttributes, String> {

  DefaultJsonMapper mapper = new DefaultJsonMapper();

  @SneakyThrows
  @Override
  public String convertToDatabaseColumn(UserAttributes attribute) {
    return mapper.toJson(attribute);
  }

  @SneakyThrows
  @Override
  public UserAttributes convertToEntityAttribute(String dbData) {
    return mapper.fromJson(dbData, UserAttributes.class);
  }
}
