package user.model;

public enum Role {
  ROOT,
  ADMIN,
  BASIC,
  GUEST
}
