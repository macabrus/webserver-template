package user.model;

import lombok.Data;

public @Data class UserAttributes {
  private String firstName;
  private String lastName;
}
