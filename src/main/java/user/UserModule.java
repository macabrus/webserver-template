package user;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;
import common.routing.Router;
import common.util.json.DefaultJsonMapper;
import common.util.json.JsonMapper;
import user.dao.DefaultUserDao;
import user.dao.UserDao;

public class UserModule extends AbstractModule {

  @Override
  protected void configure() {
    bind(UserController.class);
    // Single instance for user management module for now, could use provider later
    bind(JsonMapper.class).toInstance(new DefaultJsonMapper());
    bind(UserDao.class).to(DefaultUserDao.class);
    //install(new UserServiceModule());
    //install(new UserDaoModule());
    Multibinder.newSetBinder(binder(), Router.class).addBinding().to(UserRouter.class);
  }
}