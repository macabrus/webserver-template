package user;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import common.routing.Router;
import common.util.json.DefaultJsonMapper;
import io.javalin.Javalin;
import io.javalin.plugin.json.JavalinJackson;
import io.javalin.plugin.rendering.vue.JavalinVue;
import io.javalin.plugin.rendering.vue.VueComponent;

import java.util.HashMap;

import static io.javalin.apibuilder.ApiBuilder.*;

@Singleton
class UserRouter extends Router<UserController> {

  private Javalin javalin;

  @Inject
  public UserRouter(Javalin javalin) {
    this.javalin = javalin;
  }

  @Override
  public void bindRoutes() {
    javalin.routes(() -> {
      // TODO check if authenticated before
      // before();
      // Front-end documents
      //JavalinVue.stateFunction = ctx -> new HashMap<String, Object>(){{put("username", "sample");}};
      //get("intranet", new VueComponent("<intranet></intranet>"));
      // CRUD API for managing users
      path("user", () -> {
        get(":id", ctx -> getController().getOne(ctx, ctx.pathParam("id")));
        get(ctx -> getController().getAll(ctx));
        post(ctx -> getController().create(ctx));
        delete(":id", ctx -> getController().delete(ctx, ctx.pathParam("id")));
        post(":id", ctx -> getController().update(ctx, ctx.pathParam("id")));
      });
    });
  }
}