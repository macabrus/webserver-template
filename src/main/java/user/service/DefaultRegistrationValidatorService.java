package user.service;

import com.google.inject.Inject;
import common.dto.ValidationError;
import common.service.Service;
import lombok.extern.java.Log;
import user.dao.UserDao;
import user.dto.RegistrationForm;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Log
public class DefaultRegistrationValidatorService implements Service<RegistrationForm, List<ValidationError>> {

  @Inject
  UserDao userDao;

  @Override
  public List<ValidationError> compute(RegistrationForm input) {
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    Validator validator = factory.getValidator();
    Set<ConstraintViolation<RegistrationForm>> violations = validator.validate(input);
    List<ValidationError> errors = violations.stream().map(v -> new ValidationError(v.getPropertyPath().toString(),v.getMessage())).collect(Collectors.toList());
    if(userDao.findByUsername(input.getUsername()) != null) {
      errors.add(new ValidationError(null,"constraint.user.exists"));
    }
    return errors;
  }
}
