package user.dao;

import common.dao.Dao;
import user.model.User;

import java.util.Map;

public interface UserDao extends Dao<User> {
  User findByUsername(String username);
  void deleteByUsername(String username);
  //void update(Map<String, Object> entitiesMatching, Map<String, Object> setProperties);
}
