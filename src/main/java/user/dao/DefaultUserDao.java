package user.dao;

import common.dao.AbstractDao;
import common.util.ListMap;
import org.mindrot.jbcrypt.BCrypt;
import user.model.User;

import java.util.List;

public class DefaultUserDao extends AbstractDao<User> implements UserDao {

  @Override
  public void save(User entity) {
    if (entity.getId() != null) {
      String password = entity.getPassword();
      String hashedPw = BCrypt.hashpw(password, BCrypt.gensalt());
      entity.setPassword(hashedPw);
    }
    super.save(entity);
  }

  @Override
  public User findById(long id) {
    List<User> entities = findBy(new ListMap<String, Object>(){{put("id", id);}});
    if(entities.isEmpty())
      return null;
    return entities.get(0);
  }

  @Override
  public User findByUsername(String username) {
    List<User> users = super.findBy(new ListMap<String, Object>(){{put("username",username);}});
    if(users.isEmpty())
      return null;
    return users.get(0);
  }

  @Override
  public void delete(User entity) {
    deleteBy(new ListMap<String, Object>(){{put("id", entity.getId());}});
  }

  @Override
  public void deleteById(long id) {
    deleteBy(new ListMap<String, Object>(){{put("id", id);}});
  }

  @Override
  public void deleteByUsername(String username) {
    deleteBy(new ListMap<String, Object>(){{put("username", username);}});
  }

}
