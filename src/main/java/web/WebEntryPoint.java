package web;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import common.AppEntryPoint;
import common.routing.Router;
import common.util.json.DefaultJsonMapper;
import io.javalin.Javalin;
import io.javalin.plugin.json.JavalinJackson;
import lombok.extern.java.Log;

import java.util.Collections;
import java.util.Set;

@Log
@Singleton
class WebEntryPoint implements AppEntryPoint {
  private Javalin app;

  @Inject(optional = true)
  private Set<Router> routers = Collections.emptySet();

  @Inject
  public WebEntryPoint(Javalin app) {
    this.app = app;
  }

  @Override
  public void boot(String[] args) {
    bindRoutes();
    app.start(7000);
  }

  private void bindRoutes() {
    routers.forEach(Router::bindRoutes);
  }
}