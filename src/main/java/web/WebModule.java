package web;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.MapBinder;
import common.AppEntryPoint;
import common.EntrypointType;
import common.util.json.DefaultJsonMapper;
import io.javalin.Javalin;
import io.javalin.http.staticfiles.Location;
import io.javalin.plugin.json.JavalinJackson;
import lombok.extern.java.Log;
import org.jetbrains.annotations.NotNull;
import user.UserModule;

@Log
public class WebModule extends AbstractModule {

  private Javalin app;

  private WebModule(Javalin app) {
    this.app = app;
  }

  @NotNull
  public static WebModule create() {
    JavalinJackson.configure(new DefaultJsonMapper());
    return new WebModule(Javalin.create(config -> {
      config.addStaticFiles("static", Location.CLASSPATH);
    }));
  }

  @Override
  protected void configure() {
    //install(new AuthModule());
    install(new UserModule());
    bind(Javalin.class).toInstance(app);
    MapBinder.newMapBinder(binder(), EntrypointType.class, AppEntryPoint.class)
      .addBinding(EntrypointType.USER).to(WebEntryPoint.class);
  }
}