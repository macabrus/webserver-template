import com.google.inject.Inject;
import com.google.inject.Singleton;
import common.AppEntryPoint;
import common.EntrypointType;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

@Singleton
public class Startup {
  @Inject(optional = true)
  private Map<EntrypointType, AppEntryPoint> entrypoints = Collections.emptyMap();

  public void boot(EntrypointType entrypointType, String[] args) {
    Optional<AppEntryPoint> entryPoint = Optional.ofNullable(entrypoints.get(entrypointType));
    entryPoint.orElseThrow(() -> new RuntimeException("Entrypoint not defined")).boot(args);
  }
}